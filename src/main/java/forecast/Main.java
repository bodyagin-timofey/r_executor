package forecast;

import org.renjin.sexp.DoubleArrayVector;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by Tim on 24.10.2015.
 */
public class Main {
    public static void main(String[] args) throws ScriptException, FileNotFoundException {
        if (args.length < 2) {
            System.out.println("use agruments: path_to_function path_to_datafiles");
            return;
        }
        String pathToFunction = args[0];
        String pathToDataFiles = args[1];
        String[] files = {"B", "BCov", "eps", "epsgrp", "epsVar", "nepsgrps", "varCov", "varMean", "w"};

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("Renjin");
        if (engine == null) {
            throw new RuntimeException("Renjin Script Engine not found on the classpath.");
        }

        for (String file : files) {
            Data data = null;
            try {
                data = readFile(pathToDataFiles + file + ".csv");
                if(data.vector == null){
                    System.out.println("Program stopped");
                    return;
                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found " + pathToDataFiles + file + ".csv");
            }
            if (data == null) {
                engine.eval(file + " <- NULL");
            } else {
                engine.put(file, data.vector);
                engine.eval(String.format("dim(%s) <- c(%d, %d)", file, data.cntCols, data.cntRows));
                engine.eval(String.format("%s <- t(%s)", file, file));
            }
        }

        engine.eval(new java.io.FileReader(pathToFunction));
        engine.eval("out <- varWithErr(w, B, eps, nepsgrps, epsgrp, BCov, epsVar, varMean, varCov)");
        engine.eval("print(out)");

//        Data data = readFile("F:/ODESK/R_FUNCTION/test1/B.csv");
//        engine.put("arr", vector);
//        engine.eval("dim(arr) <- c(15, 5)");
//        engine.eval("print(arr)");
//        engine.eval(new java.io.FileReader("tst.R"));

//        engine.put("arr", a);
//        engine.eval("df <- data.frame(x=1:10, y=(1:10)+rnorm(n=10))");
//        engine.eval("df <- data.frame(x=1:10, y=(1:10)+rnorm(n=10))");
//        engine.eval("print(df)");
//        engine.eval("print(lm(y ~ x, df))");
//        engine.eval("d1 <- arr[2]");
//        engine.eval("print(d1)");
//        engine.eval(new java.io.FileReader("tst.R"));

//        engine.eval("print(vect)");
    }

    private static Data readFile(String path) throws FileNotFoundException {
        ArrayList<Double> list = new ArrayList<Double>();
        Scanner in = new Scanner(new FileReader(path));
        in.useDelimiter(",|\\r|\\n");
        in.useLocale(Locale.ENGLISH);
        int cntRows = 0;
        try {
            while (in.hasNext()) {
                if (in.hasNextDouble()) {
                    list.add(in.nextDouble());
                } else {
                    in.next();
                    cntRows++;
                }
            }
        } catch (RuntimeException e) {
            System.out.println("error reading double in file " + path);
        } finally {
            in.close();
        }
        int cntCols = list.size() / cntRows;
        if (cntCols * cntRows != list.size()) {
            System.out.println("invalid dimension in file " + path);
            return new Data(0, 0, null);
        }
        DoubleArrayVector vector = new DoubleArrayVector(list);
        return new Data(cntRows, cntCols, vector);
    }

    private static class Data {
        public Integer cntRows;
        public Integer cntCols;
        public DoubleArrayVector vector;

        public Data(Integer cntRows, Integer cntCols, DoubleArrayVector vector) {
            this.cntRows = cntRows;
            this.cntCols = cntCols;
            this.vector = vector;
        }
    }
}
